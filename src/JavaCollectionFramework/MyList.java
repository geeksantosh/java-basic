package JavaCollectionFramework;

import java.util.ArrayList;
import java.util.Iterator;

public class MyList {

    public static void main(String[] args) {
        ArrayList<String> myList = new ArrayList<>();
        myList.add("My object 1");
        myList.add("My object 2");
        myList.add("My object 3");

        String myObj = (String) myList.get(2);
        System.out.println(myObj);
        System.out.println("======================================================");

        //Normal way to print list
        int size1 = myList.size();
        System.out.println("Total size  of list is: " + size1);
        System.out.println("====================While loop==================================");

        //iterate List using while loop
        Iterator iterator = myList.iterator();
        while (iterator.hasNext()) {
            Object next = iterator.next();
            System.out.println(next);
        }

        System.out.println("======================For Loop================================");
        for (int i = 0; i < myList.size(); i++) {
            Object obj = myList.get(i);
            System.out.println(obj);
        }

        System.out.println("======================For Each loop================================");
        for (Object obj1 : myList) {
            System.out.println(obj1);
        }
    }
}
