import java.util.InputMismatchException;
import java.util.Scanner;

public class WeekDayTest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the number to print day.");
        try {
            int inputNumber = scanner.nextInt();
            switch (inputNumber) {
                case 1:
                    System.out.println("SUNDAY");
                    break;
                case 2:
                    System.out.println("MONDAY");
                    break;
                case 3:
                    System.out.println("TUESDAY");
                    break;
                case 4:
                    System.out.println("WEDNESDAY");
                    break;
                case 5:
                    System.out.println("THURSDAY");
                    break;
                case 6:
                    System.out.println("FRIDAY");
                    break;
                case 7:
                    System.out.println("SATURDAY");
                    break;
                default:
                    System.out.println("Invalid input number.");
            }
        } catch (InputMismatchException ex) {
            System.out.println("Only number from 1 to 7 are accepted.");
        }
    }
}

