package RandomPractice;

import java.util.Scanner;

public class SwitchCase {
    int choice;

    public void getInput() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Case Number");
        choice = sc.nextInt();

    }

    public void switching() {
        try {
            switch (choice) {
                case 1:
                    System.out.println("Sunday");
                    break;
                case 2:
                    System.out.println("Monday");
                    break;
                case 3:
                    System.out.println("Tuesday");
                    break;
                case 4:
                    System.out.println("Wednesday");
                    break;
                case 5:
                    System.out.println("Thursday");
                    break;
                case 6:
                    System.out.println("Friday");
                    break;
                case 7:
                    System.out.println("Saturday");
                    break;
                default:
                    System.out.println("Only valid between 1 to 7");
                    System.exit(0);
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void runAgain(){

    }

    public static void main(String[] args) {
        SwitchCase switchCase = new SwitchCase();
        switchCase.getInput();
        switchCase.switching();

    }
}
