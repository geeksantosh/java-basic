package RandomPractice;

import sun.invoke.empty.Empty;

import java.util.Scanner;
// program to find user given number is even or odd
public class EvenOdd {
    int number;
    public void getInput(){
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter Number");
            number = sc.nextInt();
        }catch (Exception e){
            System.out.println("Only Number are valid");
        }
    }

    public void findEvenOdd(){
        if (number%2 ==0){
            System.out.println("Even number");
        }else if (number%2!=0){
            System.out.println("odd number");
        }else {
            System.exit(0);
        }

    }

    public static void main(String[] args) {
        EvenOdd evenOdd = new EvenOdd();
        evenOdd.getInput();
        evenOdd.findEvenOdd();
    }
}
