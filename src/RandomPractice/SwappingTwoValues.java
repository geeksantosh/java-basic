package RandomPractice;

public class SwappingTwoValues {

    public void swapValues(){
        int a = 10;
        int b = 20;

        System.out.println("Before swapping the values are " + "A: " +a +" and " + "B: " +b );
        //Logic 1
        int temp = a;
        a = b;
        b = temp;

        System.out.println("After swapping the values are " + "A: " +a +" and " + "B: " +b );

        //Logic 2


    }


    public static void main(String[] args) {
        SwappingTwoValues swappingTwoValues = new SwappingTwoValues();
        swappingTwoValues.swapValues();

    }
}
