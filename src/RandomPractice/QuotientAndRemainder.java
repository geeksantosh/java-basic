package RandomPractice;

public class QuotientAndRemainder {
    public static void main(String[] args) {
        int dividend = 40;
        int divisor = 13;
        
        float quotient = dividend/divisor;
        System.out.println("Quotient "+quotient);
        float remainder = dividend%divisor;
        System.out.println("Remainder "+remainder);
        
    }
}
