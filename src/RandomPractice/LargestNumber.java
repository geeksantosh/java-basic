package RandomPractice;

import java.lang.reflect.Array;
import java.util.Scanner;

public class LargestNumber {
//program to compare largest value.

    public void findLargestNumber(){
        int[] arr = new int[5];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter elements");
        for (int i =0; i<arr.length; i++){
            arr[i] = scanner.nextInt();
        }
        System.out.println("Given Numbers are: ");

        for (int i : arr){
            System.out.println(i);
        }
        System.out.println("Max value");

        int max = Integer.MIN_VALUE;
        for (int i = 0; i<arr.length; i++){
            if (arr[i]>max){
                max = arr[i];
            }
        }
        System.out.println(max);

        }

    public static void main(String[] args) {
        LargestNumber largestNumber = new LargestNumber();
        largestNumber.findLargestNumber();
    }

}
