package RandomPractice;

import java.util.Scanner;

public class CheckLeapYear {
    //Program to check leap Year or not
    //century year ends with double zero (00)
    //century year is leap year when it is perfectly divisible by 400.
    int year;
    boolean leap = false;
    public void getInputYear(){
        Scanner yearSc = new Scanner(System.in);
        System.out.println("Enter Year");
        year = yearSc.nextInt();

        if (year%4 == 0){
            if (year%100 == 0){
                if (year%400 == 0){
                    leap = true;
                }else {
                    leap = false;
                }
            }else {
                leap = true;
            }
        }else {
            leap = false;
        }
        if (leap){
            System.out.println(year+" is a leap year");
        }else {
            System.out.println(year+" is not a leap year");
        }
    }

    public static void main(String[] args) {
        CheckLeapYear checkLeapYear = new CheckLeapYear();
        checkLeapYear.getInputYear();
    }
}
