package RandomPractice;

import java.util.Scanner;

public class AlphabetIsVowelOrConsonant {
   //a, e, i, o, u are vowels
   char ch;
   public void getInput(){
      System.out.println("Enter character: ");
      Scanner sc = new Scanner(System.in);
      ch = sc.next().charAt(0);

      if (ch=='a' || ch=='e' || ch=='i' || ch=='o' || ch=='u'){
         System.out.println(ch +" is vowel sound");
      }else {
         System.out.println(ch +" is Consonant");
      }
   }

   public static void main(String[] args) {
      AlphabetIsVowelOrConsonant alphabetIsVowelOrConsonant = new AlphabetIsVowelOrConsonant();
      alphabetIsVowelOrConsonant.getInput();

   }
}
