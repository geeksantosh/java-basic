package RandomPractice;

public class ArrayBasic {
    public static void main(String[] args) {

        int[] intArray = new int[5];

        intArray[0] = 55;
        intArray[1] = 50;
        intArray[2] = -27;
        intArray[3] = 25;
        intArray[4] = -22;

        int index = -1;
        for (int i = 0; i<intArray.length;i++){
            //print array
            // System.out.println(intArray[i]);

            //To find array index
            if (intArray[i] == 50){
                index = i;
                break;
            }
        }
        System.out.println("Index: "+ index);
   }
}
