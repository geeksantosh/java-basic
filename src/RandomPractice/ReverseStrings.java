package RandomPractice;

import java.util.Scanner;

public class ReverseStrings {
    String someString;

    public void getStringInput(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter text");
        someString = scanner.nextLine();
        System.out.println("Before reversing "+someString);

    }

    public void stringRev() {
        String rev = "";
        int len = someString.length();
        for (int i=len-1;i>=0;i--){
            rev = rev+someString.charAt(i);

        }
        System.out.println("After reversing "+rev);


    }

    public static void main(String[] args) {
        ReverseStrings reverseStrings = new ReverseStrings();
        reverseStrings.getStringInput();
        reverseStrings.stringRev();
    }
}
