package RandomPractice;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReverseNumbers {
    int number;

    public void getInput(){
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter a number");
            number = sc.nextInt();
        }catch (InputMismatchException e){
            System.out.println("Only numbers are allowed");
        }
    }
    public void reverseNum(){
        //Method1 using algorithm
        int rev = 0;
       while (number!=0){
           rev = rev*10 + number%10; //0*10 + 1234%10 -> 0+1234%10 = 4 ; and so on
           number = number/10; // 1234/10 = 123

       }
        System.out.println("Reversed number is: "+ rev);
    }


    public static void main(String[] args) {
        ReverseNumbers reverseNumbers = new ReverseNumbers();
        reverseNumbers.getInput();
        reverseNumbers.reverseNum();

    }
}
