package RandomPractice;
//Program to find grater number among 3

import java.util.InputMismatchException;
import java.util.Scanner;

public class LargestAmongThreeNumbers {
    int firstNumber;
    int secondNumber;
    int thirdNumber;

    public void getInput(){
        try {

            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter First Number");
            firstNumber = scanner.nextInt();

            System.out.println("Enter Second Number");
            secondNumber = scanner.nextInt();

            System.out.println("Enter Third Number");
            thirdNumber = scanner.nextInt();

            System.out.println("Given Numbers Are: " + " First Number: " + firstNumber + " Second Number: " + secondNumber +
                    " Third Number: " + thirdNumber);
        }catch (Exception ex){
            System.out.println("Only Numbers are valid " );
        }

    }

    public void compareValues() {
        try {
            if (firstNumber > secondNumber && firstNumber > thirdNumber) {
                System.out.println("Number " + firstNumber + " is greater");
            } else if (secondNumber > firstNumber && secondNumber > thirdNumber) {
                System.out.println("Number " + secondNumber + " is greater");
            } else if(thirdNumber>firstNumber&& thirdNumber>secondNumber){
                System.out.println("Number " + thirdNumber + " is greater");
            }else {
                System.exit(0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void main (String[]args){
        LargestAmongThreeNumbers largestAmongThreeNumbers = new LargestAmongThreeNumbers();
        largestAmongThreeNumbers.getInput();
        largestAmongThreeNumbers.compareValues();
    }
}
