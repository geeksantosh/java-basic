package RandomPractice;

import java.util.Scanner;

//Program to swap two numbers without using third variable
public class SwapTwoNumbers {

    public static void main(String[] args) {
        int firstNum;
        int secondNum;
        Scanner getInput = new Scanner(System.in);
        System.out.println("Enter first and second numbers");
        firstNum = getInput.nextInt();
        secondNum = getInput.nextInt();
        System.out.println("Before Swapping First Number: "+firstNum+" and Second number: "+secondNum);

        firstNum = firstNum + secondNum;
        secondNum = firstNum - secondNum;
        firstNum = firstNum - secondNum;

        System.out.println("After Swapping First Number: "+firstNum+" and Second Number: "+secondNum);
    }
}
