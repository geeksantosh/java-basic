package RandomPractice;

import java.util.Scanner;

//Java Program to Swap Two Numbers
public class SwapTwoNumbersUserGiven {
    int firstNumber;
    int secondNumber;

    public void takeInput() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter First Number");
            firstNumber = sc.nextInt();
            System.out.println("Enter Second Number");
            secondNumber = sc.nextInt();

            System.out.println("Before Swap " + " First Number " + firstNumber + " Second number: " + secondNumber);
        }catch (Exception e){
            System.out.println("Only Number are valid");
        }
    }

    public void swap(){
        int temp = firstNumber;
        firstNumber = secondNumber;
        secondNumber = temp;
        System.exit(0);

        System.out.println("After swap "+"First Number: "+firstNumber +" and Second Number: " +secondNumber );
    }

    public static void main(String[] args) {
        SwapTwoNumbersUserGiven swapTwoNumbers = new SwapTwoNumbersUserGiven();
        swapTwoNumbers.takeInput();
        swapTwoNumbers.swap();
    }
}
