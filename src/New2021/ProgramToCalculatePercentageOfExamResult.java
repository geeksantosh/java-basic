package New2021;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ProgramToCalculatePercentageOfExamResult {

    public static void main(String[] args) {
        //Get Subjects variables
        float english;
        float nepali;
        float mathematics;
        float account;
        float economics;
        float totalMarks = 500;

        //subject marks
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter Marks of English");
            english = scanner.nextFloat();
            System.out.println("Enter marks of Nepali");
            nepali = scanner.nextInt();
            System.out.println("Enter marks of Mathematics");
            mathematics = scanner.nextFloat();
            System.out.println("Enter marks of Account");
            account = scanner.nextFloat();
            System.out.println("Enter marks of Economics");
            economics = scanner.nextFloat();
            //Total achieved calculate Maks
            float totalMarksGetByStudent = english + nepali + mathematics + account + economics;
                //calculate %
                System.out.println("Marks achieved by students: " + "Nepali: "+nepali+ " English "+english+" Mathematics "+mathematics+" Account "+account);
                float totalPercentage = (totalMarksGetByStudent / totalMarks) * 100;
                System.out.println("Total marks achieved By student is: " + totalMarksGetByStudent);
                System.out.println("Total Percentage achieved by student is: " + totalPercentage);

    }
}
