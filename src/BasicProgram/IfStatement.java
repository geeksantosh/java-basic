package BasicProgram;

import java.util.Scanner;

//basic salary is 1000
// if sales grater than 10 additional bonus 200.
public class IfStatement {
    public void salaryCalculation() {

        //initialise know values
        int salary = 1000;
        int bonus = 200;

        //ask for sales
        System.out.println("How many sales did employee made?");
        try {
            Scanner scanner = new Scanner(System.in);
            int sales = scanner.nextInt();

            // calculate salary
            if (sales > 10) {
                salary = salary + bonus;
            }
        } catch (Exception ex) {
            System.out.println("Only input type number is valid for sales unit");
        }
        System.out.println("Total salary of employee is " + salary);

    }

    public static void main(String[] args) {
        IfStatement statement = new IfStatement();
        statement.salaryCalculation();
    }
}
